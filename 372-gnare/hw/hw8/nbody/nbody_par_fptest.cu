#pragma clang diagnostic push
#pragma ide diagnostic ignored "openmp-use-default-none"
/* nbody.c: parallel (CUDA/OpenMP) 2-d nbody simulation
   Author: Galen Nare

   Link this with a translation unit that defines the extern
   variables, and anim.o, to make a complete program.
 */
//#define NDEBUG // Comment this out to enable assertions

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <sys/time.h>
//#include <unistd.h>

#include "nbody.h"

extern "C" {
    #include "anim.h"
}

#define bsize 32 // this is optimal for our method, don't change it (trust me I have tried)

/* Global variables */
extern const double x_min;     /* coord of left edge of window (meters) */
extern const double x_max;     /* coord of right edge of window (meters) */
extern const double y_min;     /* coord of bottom edge of window (meters) */
extern const double y_max;     /* coord of top edge of window (meters) */
extern const int nx;           /* width of movie window (pixels) */
extern const int nbodies;      /* number of bodies */
extern const double delta_t;   /* time between discrete time steps (seconds) */
extern const int nstep;        /* number of time steps */
extern const int wstep;        /* number of times steps beween movie frames */
extern const int ncolors;      /* number of colors to use for the bodies */
extern const int colors[][3];  /* colors we will use for the bodies */
extern const Body bodies[];    /* list of bodies with initial data */
const double G = 6.674e-11;    /* universal gravitational constant */
int ny;                        /* height of movie window (pixels) */
State **states;  /* two copies of state array */
State **dev_states;
State *dev_states_big;
ANIM_File af;                  /* output anim file */
double *posbuf;               /* to send data to anim, 2*nbodies doubles */
double start_time;             /* time simulation starts */
double x_span;                 /* x-length of window (meters) */
double y_span;                 /* y-length of window (meters) */

// TODO calc best memory chunk size

const unsigned long bodies_per_section = (1 << 30) / (sizeof(State));
unsigned long bodies_per_section_r = (1 << 30) % (sizeof(State));
const unsigned long steps_per_section = bodies_per_section / nbodies;
const unsigned long steps_per_section_r = nstep % steps_per_section;

int bodies_per_device = 0;

double cpu_time_sum = 0.0, gpu_time_sum = 0.0;

int dev_count = 1;
int completed_frames = 1;
int written_frames = 0;

Body* dev_bodies;

double mytime() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return t.tv_sec + t.tv_usec / 1000000.0;
}

static inline double wrap(double x, const double min, const double max, const double span) {
    while (x<min) x+=span;
    while (x>max) x-=span;
    return x;
}

__device__ static inline double dev_wrap(double x, const double min, const double max, const double span) {
    while (x<min) x+=span;
    while (x>max) x-=span;
    return x;
}

void copy_frame(int frame_index) {
    double start = mytime();
    assert(frame_index < completed_frames);
    int err = cudaMemcpy(states[frame_index], dev_states[frame_index], nbodies * sizeof(State), cudaMemcpyDeviceToHost); // copy gpu section to host mem
    assert(err == cudaSuccess);
    cpu_time_sum += mytime() - start;
}

static void init(char *filename) {
    cudaError_t err = cudaSuccess;

    start_time = ANIM_time();
    assert(x_max > x_min && y_max > y_min);
    ny = ceil(nx * (y_max - y_min) / (x_max - x_min));
    x_span = x_max - x_min;
    y_span = y_max - y_min;
    printf("nbody: nbodies=%d nx=%d ny=%d nstep=%d wstep=%d\n",
           nbodies, nx, ny, nstep, wstep);
    const int nframes = wstep == 0 ? 0 : 1 + nstep / wstep;
    printf("nbody: creating ANIM file %s with %d frames, %zu bytes.\n",
           filename, nframes,
           ANIM_Nbody_file_size_2d(nbodies, ncolors, nframes));
    fflush(stdout);
    assert(nx >= 10 && ny >= 10);
    assert(nstep >= 1 && wstep >= 0 && nbodies > 0);
    assert(ncolors >= 1 && ncolors <= ANIM_MAXCOLORS);

    printf("Allocating\n");

    err = cudaMalloc(&dev_states, nbodies * sizeof(State));
    assert(err == cudaSuccess);

    // allocate host memory through CUDA to accelerate copying
    err = cudaHostAlloc(&states, nstep * sizeof(State*), cudaHostAllocPortable | cudaHostAllocMapped | cudaHostAllocWriteCombined);
    assert(states && err == cudaSuccess);

    # pragma omp parallel for
    for(int i = 0; i < nstep; i++) {
        states[i] = (State*) malloc(nbodies * sizeof(State));
//        err = cudaHostAlloc(&(states[i]), nbodies * sizeof(State), cudaHostAllocPortable | cudaHostAllocMapped | cudaHostAllocWriteCombined);
//        if (!states[i] || err != cudaSuccess) {
//            printf("%s\n", cudaGetErrorString(err));
//            exit(-2);
//        }
    }

    // Pointer trickery to help addressing and allocating device array (and space locality)
    dev_states = (State**) malloc(nstep * sizeof(State*));
    assert(dev_states);

    size_t pitch; // don't care what happens with this

    err = cudaMallocPitch(&dev_states_big, &pitch, nstep * nbodies * sizeof(State), 1); // allocating w/ pitch aligns the array in a more optimal way
    if (!dev_states_big || err != cudaSuccess) {
        printf("%s\n", cudaGetErrorString(err));
        exit(-2);
    }

    # pragma omp parallel for
    for(int i = 0; i < nstep; i++) {
        dev_states[i] = &(dev_states_big[i * nbodies]);
    }

    err = cudaMalloc(&dev_bodies, nbodies * sizeof(Body));
    assert(dev_bodies && err == cudaSuccess);

    err = cudaMemcpy(dev_bodies, bodies, nbodies * sizeof(Body), cudaMemcpyHostToDevice);
    assert(err == cudaSuccess);

    cudaGetDeviceCount(&dev_count);

    bodies_per_device = nbodies / dev_count;

    posbuf = (double*) malloc(2 * nbodies * sizeof(double));
    assert(posbuf);

    int radii[nbodies], bcolors[nbodies];
    ANIM_color_t acolors[ncolors]; // RGB colors converted to ANIM colors

    # pragma omp parallel for
    for (int i = 0; i < nbodies; i++) {
        assert(bodies[i].mass > 0);
        assert(bodies[i].color >= 0 && bodies[i].color < ncolors);
        assert(bodies[i].radius > 0);
        states[0][i] = bodies[i].state;
        radii[i] = bodies[i].radius;
        bcolors[i] = bodies[i].color;
    }

    err = cudaMemcpy(dev_states[0], states[0], nbodies * sizeof(State), cudaMemcpyHostToDevice);
    assert(err == cudaSuccess);

    written_frames++;

    for (int i = 0; i < ncolors; i++) {
        acolors[i] = ANIM_Make_color(colors[i][0], colors[i][1], colors[i][2]);
    }

    af = ANIM_Create_nbody_2d(nx, ny, x_min, x_max, y_min, y_max,
                     nbodies, radii, ncolors, acolors, bcolors, filename);

    cudaDeviceSynchronize();
}

static void write_frame(int frame_index) {
    int j = 0;
    for (int i = 0; i < nbodies; i++) {
        posbuf[j++] = states[frame_index][i].x;
        posbuf[j++] = states[frame_index][i].y;
    }
    ANIM_Write_frame(af, posbuf);
}

void cpu_kernel_update(int dev_nbodies, int dev_bodies_per, int frame, int device_num, double dev_delta_t,
                                  double dev_G, State* dev_states, State* dev_states_new, const Body* dev_bodies, double dev_x_min,
                                  double dev_x_max, double dev_y_min, double dev_y_max, double dev_x_span,
                                  double dev_y_span) {
    for (int i = 0; i < dev_nbodies; i++) {
//    int i = device_num * dev_bodies_per + blockIdx.x * bsize + threadIdx.x; // TODO sort out proper indexing with multi device
        if (i >= dev_nbodies) {
            return;
        }

        double x = dev_states[i].x, y = dev_states[i].y;
        double vx = dev_states[i].vx, vy = dev_states[i].vy;
        // ax times delta t, ay times delta t...
        double ax_delta_t = 0.0, ay_delta_t = 0.0;

        for (int j = 0; j < dev_nbodies && j != i; j++) {
            const double dx = dev_states[j].x - x, dy = dev_states[j].y - y;
            const double mass = dev_bodies[j].mass;
            const double r_squared = dx * dx + dy * dy;

            if (r_squared != 0) {
                const double r = sqrt(r_squared);

                if (r != 0) {
                    const double acceleration = dev_G * mass / r_squared;
                    const double atOverr = acceleration * dev_delta_t / r;

                    ax_delta_t += dx * atOverr;
                    ay_delta_t += dy * atOverr;
                }
            }
        }

        vx += ax_delta_t;
        vy += ay_delta_t;
        x += dev_delta_t * vx;
        y += dev_delta_t * vy;
//    assert(!isnan(y) && !isnan(vy) && !isnan(x) && !isnan(vx));
        x = wrap(x, dev_x_min, dev_x_max, dev_x_span);
        y = wrap(y, dev_y_min, dev_y_max, dev_y_span);
        dev_states_new[i] = (State) {x, y, vx, vy};
    }
}

__global__ void gpu_kernel_update(int dev_nbodies, int dev_bodies_per, int frame, int device_num, double dev_delta_t,
                                  double dev_G, State* dev_states, State* dev_states_new, Body* dev_bodies, double dev_x_min,
                                  double dev_x_max, double dev_y_min, double dev_y_max, double dev_x_span,
                                  double dev_y_span) {
    int i = device_num * dev_bodies_per + blockIdx.x * bsize + threadIdx.x; // TODO sort out proper indexing with multi device
    if (i >= dev_nbodies) {
        return;
    }

    double x = dev_states[i].x, y = dev_states[i].y;
    double vx = dev_states[i].vx, vy = dev_states[i].vy;
    // ax times delta t, ay times delta t...
    double ax_delta_t = 0.0, ay_delta_t = 0.0;

    for (int j = 0; j < dev_nbodies && j != i; j++) {
        const double dx = dev_states[j].x - x, dy = dev_states[j].y - y;
        const double mass = dev_bodies[j].mass;
        const double r_squared = dx * dx + dy * dy;

        if (r_squared != 0) {
            const double r = sqrt(r_squared);

            if (r != 0) {
                const double acceleration = dev_G * mass / r_squared;
                const double atOverr = acceleration * dev_delta_t / r;

                ax_delta_t += dx * atOverr;
                ay_delta_t += dy * atOverr;
            }
        }
    }

    vx += ax_delta_t;
    vy += ay_delta_t;
    x += dev_delta_t * vx;
    y += dev_delta_t * vy;
//    assert(!isnan(y) && !isnan(vy) && !isnan(x) && !isnan(vx));
    x = dev_wrap(x, dev_x_min, dev_x_max, dev_x_span);
    y = dev_wrap(y, dev_y_min, dev_y_max, dev_y_span);
    dev_states_new[i] = (State) {x, y, vx, vy};
}

/* Move forward one time step.  This is the "integration step".  For
   each body b, compute the total force acting on that body.  If you
   divide this by the mass of b, you get b's acceleration.  So you
   actually just calculate b's acceleration directly, since this is
   what you want to know.  Once you have the acceleration, update the
   velocity, then update the position. */
static void update() {
            if (completed_frames < nstep) {
                double start = mytime();

//                printf("here\n");
//                fflush(stdout);
                cpu_kernel_update(nbodies, bodies_per_device, completed_frames, 0, delta_t, G,
                                                                              states[completed_frames - 1],
                                                                              states[completed_frames],
                                                                              bodies, x_min, x_max, y_min,
                                                                              y_max, x_span, y_span);

                #pragma omp parallel for
                for (int i = 0; i < dev_count; i++) {
                    int err = cudaSetDevice(i);
                    assert(err == cudaSuccess);
                    gpu_kernel_update<<<(bodies_per_device / bsize) + 1, bsize>>>(nbodies, bodies_per_device,
                                                                                  completed_frames, i, delta_t, G,
                                                                                  dev_states[completed_frames - 1],
                                                                                  dev_states[completed_frames],
                                                                                  dev_bodies, x_min, x_max, y_min,
                                                                                  y_max, x_span, y_span);
                }


                cudaDeviceSynchronize();

                State* test_dev_states = (State*) malloc(nbodies * sizeof(State));
                int err = cudaMemcpy(test_dev_states, dev_states[completed_frames], nbodies * sizeof(State), cudaMemcpyDeviceToHost); // copy gpu section to host mem
                assert(err == cudaSuccess);
                for (int i = 0; i < nbodies; i++) {
                    if (test_dev_states[i].x != states[completed_frames][i].x) {
                        printf("State %d differs on frame %d: device x: %0.10f; host x: %0.10f\n", i, completed_frames, test_dev_states[i].x, states[completed_frames][i].x);
                        fflush(stdout);
                    }
                    if (test_dev_states[i].y != states[completed_frames][i].y) {
                        printf("State %d differs on frame %d: device y: %0.10f; host y: %0.10f\n", i, completed_frames, test_dev_states[i].y, states[completed_frames][i].y);
                        fflush(stdout);
                    }
                    if (test_dev_states[i].vx != states[completed_frames][i].vx) {
                        printf("State %d differs on frame %d: device vx: %0.10f; host vx: %0.10f\n", i, completed_frames, test_dev_states[i].vx, states[completed_frames][i].vx);
                        fflush(stdout);
                    }
                    if (test_dev_states[i].vy != states[completed_frames][i].vy) {
                        printf("State %d differs on frame %d: device vy: %0.10f; host vy: %0.10f\n", i, completed_frames, test_dev_states[i].vy, states[completed_frames][i].vy);
                        fflush(stdout);
                    }
                }
                free(test_dev_states);

                completed_frames++;

                gpu_time_sum += mytime() - start;
            }
}

/* Close GIF file, free all allocated data structures */
static void wrapup() {
    printf("Cleaning up\n");
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            ANIM_Close(af);
        }
        #pragma omp section
        {
            free(posbuf);
        }
        #pragma omp section
        {
            for (int i = 0; i < nstep; i++) {
                free(states[i]);
//                cudaFreeHost(states[i]);
            }
            cudaFreeHost(states);
        }
        #pragma omp section
        {
            for (int i = 0; i < nstep; i++) {
                cudaFree(dev_states[i]);
            }
            free(dev_states);
        }
        #pragma omp section
        {
            cudaFree(dev_bodies);
        }
    }
    printf("\nnbody: finished.  Time = %lf\n", ANIM_time() - start_time);
}

/* Two arguments: the name of the output file and num. threads */
int main(int argc, char *argv[]) {
    int statbar = 0; // used for printing status updates

    unsigned long free, total;
    cudaMemGetInfo(&free, &total);
    printf("GPU memory free: %lu MiB, total: %lu MiB\n", free / (1 << 20), total / (1 << 20));

    unsigned long nbytes = nbodies * nstep * sizeof(State);
    printf("Expected memory usage on both device and host: %lu MiB\n", nbytes / (1 << 20));

    assert(argc == 3);
    init(argv[2]); // arg 1: ncores is ignored, we will just use them all
    printf("Generating\n");
    for (int i = 1; i <= nstep; i++) {
        update();
        ANIM_Status_update(stdout, nstep, i, &statbar);
    }

    printf("\nSyncing\n");
    cudaDeviceSynchronize();

    printf("Copying\n");
    #pragma omp parallel for
    for (int i = 1; i < nstep; i++) {
        copy_frame(i);
    }

    statbar = 0;

    for (int i = 0; i < nstep; i++) {
//        for (int j = 0; j < nbodies; j++) {
//            printf("[%d][%d]: %.2f, %.2f\n", i, j, states[i][j].x, states[i][j].x);
//        }
//        fflush(stdout);
        ANIM_Status_update(stdout, nstep, i, &statbar);
        if (wstep != 0 && i % wstep == 0) write_frame(i);
    }
    write_frame(nstep - 1);
    printf("\n");

    wrapup();

    double cpu_avg = cpu_time_sum / nstep * 1000000.0;
    double gpu_avg = gpu_time_sum / nstep * 1000000.0;
    printf("Average time for GPU update: %0.3fus, CPU copy: %0.3fus\n", gpu_avg, cpu_avg);
}

#pragma clang diagnostic pop