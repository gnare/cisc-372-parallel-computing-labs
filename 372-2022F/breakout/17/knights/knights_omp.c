
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <omp.h>

char * dub(char * name) {
  char * new_name = malloc(strlen(name) + 5);
  memcpy(new_name, "Sir ", 4);
  strcpy(new_name + 4, name);
  return new_name;
}

int main(int argc, char * argv[]) {
  int n = argc - 1;
  if (n >= 1) {
    char * knights[n];
#pragma omp parallel
    {
	    int numthreads = omp_get_num_threads();
	    int id = omp_get_thread_num();
	    for (int i=id; i<n; i+=numthreads)
		    knights[i] = dub(argv[i+1]);
    }
    for (int i=0; i<n; i++)
      printf("%s\n", knights[i]);
#pragma omp parallel
    {
	    int numthreads = omp_get_num_threads();
            int id = omp_get_thread_num();
	    for (int i=id; i<n; i+=numthreads)
		    free(knights[i]);
    }
  }
}
