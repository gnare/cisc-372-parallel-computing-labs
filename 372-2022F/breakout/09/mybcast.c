#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char* argv[]) {
  int rank,nprocs;
  int size = argc-1;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);

  MPI_Bcast(&size,1,MPI_INT,0,MPI_COMM_WORLD);
  int* nums = malloc(size*sizeof(int));
  
  if (rank == 0) {
    for (int i = 0; i < argc-1; i++) {
      nums[i] = atoi(argv[i+1]);
    }
  }
  MPI_Bcast(nums,size,MPI_INT,0,MPI_COMM_WORLD);

    printf("Proccess %d received: ",rank);
    for (int i = 0; i < size; i++) {
      printf("%d ",nums[i]);
    }
    printf("\n");

  free(nums);
  MPI_Finalize();
}
