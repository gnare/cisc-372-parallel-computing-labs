      Breakout for Class 19: OpenMP loop strategies

Begin by copying this directory to your personal repo,
breakout/19.

Work with other people at your table.

Take a look at matmul.c. This is a simple matrix multiplication
program.  It takes 3 command line args: L, M, and N (ints).  It
constructs a random NxL matrix a, a random LxM matrix b, and then
multiplies the two matrices to yield an NxM matrix c.  Compiling with
-DDEBUG turns on printing---this is good for small values of L, M, and
N but not when those parameters are large.  Understand this code,
compile it, and run it with various values for L, M, and N, with
and without debugging.

1. Come up with a simple "Big O" expression for the amount of computation
performed by the program, in terms of L, M, and N.

2. Create a Makefile to compile, run some tests, and clean.
 
3. Now write a parallel version using OpenMP.  Call it matmul_omp.c.
Review the various options for OpenMP for loops and discuss them.

Experiment with different strategies. There is a triply nested loop,
so you can try parallelizing the outer loop, or the middle loop, or
the inner-most loop. You can try "collapsing" some or all of these
loops. You can try static scheduling, dynamic scheduling, and guided
scheduling. You can try different chunk sizes.

Update your Makefile to compile and build the parallel version
and run one or more experiements.

When you do your timings, compile without the DEBUG flag --- you don't
want to print huge matrices to your screen. Find "good" values for
L,M,N that let the program run for a reasonable but not trivial amount
of time. Use a good machine, like Bridges2 with one full RM node.
Beowulf (cisc372) might be OK too.

Try timing with different values for the parameters.  Is it possible
that some strategies work better for certain parameter values, and
other strategies work better for others?  Can you characterize or
describe the reasons?

Feel free to commit more than one version of the parallel code
and update your Makefile as needed.

Write up your conclusions in a file README.txt and commit it.
