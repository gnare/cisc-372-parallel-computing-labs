#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

// https://google.github.io/styleguide/cppguide.html

double mytime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return t.tv_sec + t.tv_usec / 1000000.0;
}

int is_perfect(int n) {
  if (n < 2) {
    return 0;
  }
  int sum = 1, i = 2;
  while (1) {
    const int i_squared = i * i;
    if (i_squared < n) {
      if (n % i == 0) {
        sum += i + n / i;
      }
      i++;
    } else {
      if (i_squared == n) {
        sum += i;
      }
      break;
    }
  }
  return sum == n;
}

int main(int argc, char** argv) {
  int rank, nprocs;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  int num_perfect = 0;
  double start_time = mytime();
  
  if (argc != 2) {
    printf("Usage: perfect.exec bound\n");
    exit(1);
  }

  // using cyclic distribution: process gets every nth number to test (where n = nprocs).
  int bound = atoi(argv[1]); 
  for (int i = 1 + rank; i <= bound; i += nprocs) {
    if (i % 1000000 == 0) {
      printf("i = %d\n", i);
      fflush(stdout);
    }

    if (is_perfect(i)) {
      printf("Found a perfect number: %d\n", i);
      num_perfect++;
    }
  }

  double runtime = mytime() - start_time;
  double max_runtime = 0.0;

  printf("Number of perfect numbers less than or equal to %d: %d.  Time = %lf\n", 
    bound, num_perfect, runtime);

  MPI_Reduce(&runtime, &max_runtime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD); // get runtime

  if (rank == 0) {
    fprintf(stderr, "%d %f\n", nprocs, max_runtime);
  }

  MPI_Finalize();
}
