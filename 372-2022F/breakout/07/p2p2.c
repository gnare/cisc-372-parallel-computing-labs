#include <stdio.h>
#include <string.h>
#include <mpi.h>

int main() {
  int rank;
  char message[150];

  MPI_Init(NULL,NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 1) {
    strcpy(message, "Greetings to proc 3 from proc 1!");
    MPI_Send(message, strlen(message)+1,MPI_CHAR,3,9, MPI_COMM_WORLD);
  } else if (rank == 3) {
    MPI_Recv(message, 150, MPI_CHAR, 1,9, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("Received from proc 3: %s\n",message);
  }
  MPI_Finalize();
}
