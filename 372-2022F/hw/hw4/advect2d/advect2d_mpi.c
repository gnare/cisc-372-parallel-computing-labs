#include <stdlib.h>
#include <assert.h>
#include <mpi.h>
#include "mpianim.h"

const int n = 200;         // number of discrete points including endpoints
const double k = 0.01;     // D*dt/(dx*dx), diffusivity constant
const double c = 0.001;    // advection constant
const int nstep = 200000;  // number of time steps
const int wstep = 400;     // time between writes to file
const double m = 100.0;    // initial temperature of interior
char * filename = "advect2d_mpi.anim";  // name of file to create
double ** u, ** u_new;     // two copies of the temperature function
MPIANIM_File af;           // output file
double start_time;         // time simulation starts
int nprocs, rank;
int first, nl;
int left, right;

static void setup(void) {
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  first = rank*n/nprocs;
  nl = (rank+1)*n/nprocs - first;
  start_time = ANIM_time();
  u = ANIM_allocate2d(nl+2,n);
  u_new = ANIM_allocate2d(nl+2,n);
  for (int i = 0; i < nl+2; i++)
    for (int j = 0; j < n; j++)
      u_new[i][j] = u[i][j] = 0.0;
  af = MPIANIM_Create_heat_2d
    (n, n, 0, 1, 0, 1, 0, m,
     nl, n, first, 0, filename, MPI_COMM_WORLD);
}

static void teardown(void) {
  MPIANIM_Close(af);
  ANIM_free2d(u);
  ANIM_free2d(u_new);
  if (rank == 0)
    printf("\n");
}

static void exchange() {
}

static void update() {
}

int main() {
  int dots = 0;
  MPI_Init(NULL, NULL);
  setup();
  if (wstep != 0)
    MPIANIM_Write_frame(af, &u[1][0], MPI_STATUS_IGNORE);
  for (int i = 1; i <= nstep; i++) {
    exchange();
    update();
    if (rank == 0)
      ANIM_Status_update(stdout, nstep, i, &dots);
    if (wstep != 0 && i%wstep == 0)
      MPIANIM_Write_frame(af, &u[1][0], MPI_STATUS_IGNORE);
  }
  teardown();
  MPI_Finalize();
}
