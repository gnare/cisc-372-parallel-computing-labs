#!/bin/bash
#SBATCH -p RM
#SBATCH -t 00:05:00
#SBATCH -N 1
#SBATCH --ntasks-per-node 64
# echo commands to stdout
set -x
mpirun -np $SLURM_NTASKS ./diffusion2d_mpi.exec 20000 15000 0.2 1000 1000 /ocean/projects/see200002p/sfsiegel/d2d_mpi_big_64.anim
