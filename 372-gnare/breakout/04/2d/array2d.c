#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

double* create(int n, int m) {
	double* arr = calloc(n * m, sizeof(double));
	return arr;
}

void init(double* arr, int n, int m) {
    for (int i = 0; i < n * m; i++) {
		if (i < m || i % m == 0 || i % m == m - 1 || i > n * (m - 1) - 1) {
			arr[i] = 100.0;
		} else {
			arr[i] = 0.0;
		}
	}
}

void destroy(double* arr) {
	free(arr);
}

void print(double* arr, int n, int m) {
	for (int i = 0; i < n * m; i++) {
		if (i % m == 0) {
			printf("\n");
		}
		printf("%5.1f ", arr[i]);
	}
	printf("\n");
}

int main(int argc, char** argv) {
	assert(argc == 3);
	int n = atoi(argv[1]);
	assert(n > 0);
	int m = atoi(argv[2]);
	assert(m > 0);
	double* arr = create(n, m);
	init(arr, n, m);
	print(arr, n, m);
	destroy(arr);
}
