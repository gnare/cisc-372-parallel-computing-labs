#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

char message[40] = "Greetings to proc 3 from proc 1!\n";

int main(int argc, char** argv) {
  int rank, nprocs;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (nprocs != 4) {
    fprintf(stderr, "Incorrect nprocs.\n");
    return -4;
  }
  
  if (rank == 1) {
    MPI_Send(message, 40, MPI_CHAR, 3, 4, MPI_COMM_WORLD);
  } else if (rank == 3) {
    char* b = calloc(40, sizeof(char));
    MPI_Recv(b, 40, MPI_CHAR, 1, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("%s\n", b);
    free(b);
  }

  MPI_Finalize();
}
