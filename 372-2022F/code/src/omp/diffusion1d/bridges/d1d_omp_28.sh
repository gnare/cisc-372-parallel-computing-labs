#!/bin/bash
#SBATCH -p RM
#SBATCH -t 00:10:00
#SBATCH -N 1
#SBATCH --ntasks-per-node 28
set -x
OMP_NUM_THREADS=28 ./diffusion1d_omp.exec 100000000 0.2 1000 1000 big.anim

