
#include <mpi.h>
#include <assert.h>
#include <stdlib.h>

int rank, nprocs;

void barrier() {
  for (int i=1; i<nprocs; i*=2) {
    MPI_Sendrecv(NULL, 0, MPI_INT, (rank+i)%nprocs, 9,
		 NULL, 0, MPI_INT, (rank+nprocs-i)%nprocs, 9,
		 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  }
}

int main() {
  MPI_Init(NULL, NULL);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int p = 0;
  for (int t=0; t<4; t++) {
    if (rank==0) {
      int buf;
      for (int i=1; i<nprocs; i++) {
	MPI_Recv(&buf, 1, MPI_INT, MPI_ANY_SOURCE, 0,
		 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	assert(buf==p);
      }
    } else {
      MPI_Send(&p, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
    p=1-p;
    barrier();
  }
  MPI_Finalize();
}
