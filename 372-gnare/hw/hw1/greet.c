#include <stdio.h>

void greet_recur(int);

int main() {
	greet_recur(12);
}

void greet_recur(int n) {
	if (n > 0) {
		printf("Greetings from Galen!\n");
		greet_recur(n - 1);
	}
}
