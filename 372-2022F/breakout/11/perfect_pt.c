/* perfect.c: find all perfect numbers up to a bound */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>

int num_perfect = 0, bound = 0, nthreads = 0;

double mytime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return t.tv_sec + t.tv_usec/1000000.0;
}

_Bool is_perfect(int n) {
  if (n < 2) return 0;
  int sum = 1, i = 2;
  while (1) {
    const int i_squared = i*i;
    if (i_squared < n) {
      if (n%i == 0) sum += i + n/i;
      i++;
    } else {
      if (i_squared == n) sum += i;
      break;
    }
  }
  return sum == n;
}

void* thread_f(void * arg){
  int tnum = * ((int*)arg);		
  for (int i=tnum; i<=bound; i+=nthreads) {
    if (i%1000000 == 0) {
      printf("i = %d\n", i);
      fflush(stdout);
    }
    if (is_perfect(i)) {
      printf("Thread %d found a perfect number: %d\n", tnum, i);
      num_perfect++;
    }
  }
}

int main(int argc, char * argv[]) {
  double start_time = mytime();
  
  if (argc != 3) {
    printf("Usage: perfect.exec bound\n");
    exit(1);
  }

  bound = atoi(argv[1]);
  nthreads = atoi(argv[2]);

  pthread_t threads[nthreads]; 
  int tids[nthreads];

  // get list of thread ids
  for (int i = 0; i < nthreads; i++)
    tids[i] = i;
  
  // create threads
  for (int i = 0; i < nthreads; i++)
    pthread_create(threads + i, NULL, thread_f, tids + i);
  
  // join threads
  for (int i = 0; i < nthreads; i++)
    pthread_join(threads[i], NULL);

  printf("Number of perfect numbers less than or equal to %d: %d.  Time = %lf\n",
	 bound, num_perfect, mytime() - start_time);
}
