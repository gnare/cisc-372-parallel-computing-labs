#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

double s = 0.0;

pthread_mutex_t mp = PTHREAD_MUTEX_INITIALIZER;

pthread_t threads[8];

double vals[10000];

void find_max(int len, double* a, int nthreads);
void* runner(void* data);


typedef struct max_thread_data_t {
  int len;
  double* a;
  int nthreads;
  int rank;
} max_thread_data_t;

int main() {
  // generate doubles
  srand(143985402);
  for (int i = 0; i < 10000; i++) {
    vals[i] = (double) rand();
  }

  find_max(10000, vals, 8);

  printf("%.6f\n", s);
}

void* runner(void* data) {
	max_thread_data_t* data_struct = data;

  int first = (data_struct->len / data_struct->nthreads) * data_struct->rank;
  int last = (data_struct->len / data_struct->nthreads) * (data_struct->rank + 1);

  double lmax = 0.0;

  for (int i = first; i < last; i++) {
    if (data_struct->a[i] > lmax) {
      lmax = data_struct->a[i];
    }
  }

  pthread_mutex_lock(&mp);
  if (lmax > s) {
    s = lmax;
  }
  pthread_mutex_unlock(&mp);

  free(data_struct);
}

void find_max(int len, double* a, int nthreads) {
	for (int i = 0; i < nthreads; i++) {
		max_thread_data_t* data = calloc(1, sizeof(max_thread_data_t));
		data->rank = i;
		data->len = len;
		data->a = a;
		data->nthreads = nthreads;
		
		pthread_create(threads + i, NULL, runner, data);
	}
	
	for (int i = 0; i < nthreads; i++) { 
		pthread_join(threads[i], NULL);
	}
}
