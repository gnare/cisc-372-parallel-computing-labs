/* Incorrect synchronization.  Use mutexes and condition variables
 * to fix. */
#include <stdio.h>
#include <pthread.h>

int s = 0;

pthread_cond_t condS1;
pthread_mutex_t mutex;

void *f1(void *arg) {
    pthread_mutex_lock(&mutex);
    printf("Thread 1: waiting for signal.\n");
    fflush(stdout);
    // wait for s!=0 here
    pthread_cond_wait(&condS1, &mutex);
    printf("Thread 1: signal received: s=%d\n", s);
    fflush(stdout);
    pthread_mutex_unlock(&mutex);
    return NULL;
}

void *f2(void *arg) {
    pthread_mutex_lock(&mutex);
    printf("\tThread 2: sending signal.\n");
    fflush(stdout);
    s = 1;
    pthread_mutex_unlock(&mutex);
    pthread_cond_signal(&condS1);
    return NULL;
}

int main(void) {
    pthread_t t1, t2;

    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&condS1, NULL);

    pthread_create(&t1, NULL, f1, NULL);
    pthread_create(&t2, NULL, f2, NULL);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
}
