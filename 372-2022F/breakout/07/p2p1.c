#include<stdio.h>
#include<mpi.h>

int main() {
  int message, rank;
  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 1) {
    message = 497;
    MPI_Send(&message, 1, MPI_INT, 3, 47, MPI_COMM_WORLD);
  } else if (rank == 3) {
    MPI_Recv(&message, 1, MPI_INT, 1, 47, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("Proc 3 recieved: %d\n", message);
  }
  MPI_Finalize();
}
