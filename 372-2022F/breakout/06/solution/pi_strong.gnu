set terminal pdf # size 4,4

set output "pi_mpi.pdf"
set xlabel center "Number of processes"
set ylabel center "time (seconds)"
set xr [0:12]
set yr [0:10]
plot "pi_strong.dat" using 1:2 title 'MPI' with linespoints

set output "pi_speedup.pdf"
set xlabel center "Number of processes"
set ylabel center "speedup"
set xr [0:12]
set yr [0:12]
first(x) = ($0 > 0 ? base : base = x)
plot "pi_strong.dat" using 1:(first($2), base/$2) title 'Speedup' with linespoints

set output "pi_efficiency.pdf"
set xlabel center "Number of processes"
set ylabel center "efficiency"
set xr [0:12]
set yr [0:1]
first(x) = ($0 > 0 ? base : base = x)
plot "pi_strong.dat" using 1:(first($2), base/($2*$1)) title 'Efficiency' with linespoints
