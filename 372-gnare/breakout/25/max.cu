#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>

const int blocksize = 1024;

double mytime() {
	struct timeval t;
	gettimeofday(&t, NULL);
	return t.tv_sec + t.tv_usec / 1000000.0;
}

__shared double max = -2.0;
__shared double grid_max[256];

__global void sin_max(long n, double* dev_block) {
	int x = blockDim.x * blocksize + blockIdx.x;
	
	for (; x < n && (x - (blockDim.x * blocksize)) < blocksize; x++) {
		dev_block[x % blocksize] = sin(x);
	}
}

int main(int argc, char** argv) {
	assert(argc > 1);
	int exp = atoi(argv[1]);
	long n = pow(2, exp);

	double temp_vals[blocksize];
	
	double* dev_block;
	cudaMalloc(&dev_block, blocksize * sizeof(double));
// todo
	cudaMemcpy();
	cudaFree(dev_block);
}
