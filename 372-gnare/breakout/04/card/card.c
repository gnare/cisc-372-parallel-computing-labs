#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

enum Suit {
	Spades = 0,
	Diamonds = 1,
	Clubs = 2,
	Hearts = 3
};

const char rank_strs[15][7]  = {"NULL", "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten",
	"Jack", "Queen", "King"};

struct Card {
	enum Suit suit;
	int rank;
} Card;

struct Card** make_deck() {
	struct Card** deck = calloc(52, sizeof(struct Card*));
	for (int s = 0; s < 4; s++) {
		for (int i = 1; i < 14; i++) {
			int idx = s * 13 + i;
			deck[idx] = calloc(1, sizeof(struct Card));
			deck[idx]->suit = s;
			deck[idx]->rank = i;
		}
	}
	return deck;
}

int main(int argc, char** argv) {
	
}
