/* perfect.c: find all perfect numbers up to a bound */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdbool.h>



double mytime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return t.tv_sec + t.tv_usec/1000000.0;
}

__device__ bool is_perfect(int n) {
  if (n < 2) return 0;
  int sum = 1, i = 2;
  while (1) {
    const int i_squared = i*i;
    if (i_squared < n) {
      if (n%i == 0) sum += i + n/i;
      i++;
    } else {
      if (i_squared == n) sum += i;
      break;
    }
  }
  return sum == n;
}

__global__ void kernal(int bound){
	int num_perfect;
	int num_blocks = gridDim.x;
	int block_id = blockIdx.x;
	int num_threads = blockDim.x;
	int local_id = threadIdx.x;

	int global_threads = num_blocks * num_threads;
	int global_id = num_threads * block_id + local_id;

  for (int i=global_id; i<=bound; i+=global_threads) {
    if (i%1000000 == 0) {
      //printf("i = %d\n", i);
    }
    if (is_perfect(i)) {
      printf("Found a perfect number: %d\n", i);
      num_perfect++;
    }
  }
	
}

int main(int argc, char * argv[]) {
  double start_time = mytime();
  
  if (argc != 2) {
    printf("Usage: perfect.exec bound\n");
    exit(1);
  }
  int bound = atoi(argv[1]);
  kernal<<<100, 1024>>>(bound);
  cudaDeviceSynchronize();
}
