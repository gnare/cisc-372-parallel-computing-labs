//
// Created by gnare on 12/9/22.
//

#include <stdio.h>
#include "nbody.h"

extern const int nbodies;      /* number of bodies */
extern const int nstep;

int main() {
    unsigned long nbytes = nbodies * nstep * sizeof(State);
    printf("%d states * %d timesteps * %lu bytes per state = %lu bytes\n", nbodies, nstep,
           sizeof(State), nbytes);

    printf("%.2f GB\n", ((double) nbytes) / 1e9);
    printf("or %lu blocks of 1 GiB\n", (nbytes / (1 << 30)) + 1);

    unsigned long bodies_per_block = (1 << 30) / (sizeof(State));
    unsigned long bodies_per_block_r = (1 << 30) % (sizeof(State));
    printf("%lu bodies per block (rem %lu)\n", bodies_per_block, bodies_per_block_r);

    unsigned long steps_per_block = bodies_per_block / nbodies;
    unsigned long bodies_per_section_r = bodies_per_block % nbodies;
    unsigned long steps_per_block_r = nstep % steps_per_block;
    printf("%lu steps per block (rem %lu steps, %lu bodies)\n", steps_per_block, steps_per_block_r, bodies_per_section_r);

    const int n_gpus = 8;
    printf("With %d GPUs: %d bodies per GPU, per step (rem %d)\n", n_gpus, nbodies / n_gpus, nbodies % n_gpus);
}