//
// Created by gnare on 2022-11-29.
//
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#inculde <assert.h>

double mytime() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return t.tv_sec + t.tv_usec/1000000.0;
}

__global__ void mat_shrink(int n, int m, double * old_mat, double * new_mat) {
    const int x = blockDim.x*blockIdx.x + threadIdx.x;
    const int y = blockDim.y*blockIdx.y + threadIdx.y;

    double avg = 0.0;
    if (x < n && y < m) {
        int idx = (x * m * 2) + (y * 2);
        avg += old_mat[idx];

        idx = ((x * 2 + 1) * m) + (y * 2);
        avg += old_mat[idx];

        idx = (x * 2 * m) + ((y + 1) * 2);
        avg += old_mat[idx];

        idx = ((x * 2 + 1) * m) + ((y + 1) * 2);
        avg += old_mat[idx];

        avg /= 4.0;

        idx = x*m + y;
        new_mat[idx] = avg;
    }
}

int main(int argc, char** argv) {
    if (argc < 3) {
        return 1;
    }

    int m = atoi(argv[1]);
    int n = atoi(argv[2]);
    assert(m > 1 && n > 1);

    double* mat_big = calloc(m * n, sizeof(double));
    int c = m * n;
    for (int i = 0; i < c; i++) {
        mat_big[i] = i;
    }

    double* mat_small = calloc((m / 2) * (n / 2), sizeof(double));
    printf("Host initialization complete.\n");
    fflush(stdout);

    double * big_mat_dev, * small_mat_dev, start_time = mytime();

    err = cudaMalloc((void**)&big_mat_dev, n * m * sizeof(double));
    assert(err == cudaSuccess);
    err = cudaMalloc((void**)&small_mat_dev, (m / 2) * (n / 2) * sizeof(double));
    assert(err == cudaSuccess);
    err = cudaMemcpy(big_mat_dev, mat_big, n * m * sizeof(double), cudaMemcpyHostToDevice);
    assert(err == cudaSuccess);
    printf("Device initialization complete.\n");
    fflush(stdout);

    mat_shrink<<gridDim, blockDim>>(n / 2, m / 2, big_mat_dev, small_mat_dev);
    cudaMemcpy(mat_small, small_mat_dev, (n / 2) * (m / 2) * sizeof(double), cudaMemcpyDeviceToHost);
    printf("Result obtained.  Time: %lf\n", mytime() - start_time);

    free(mat_big);
    free(mat_small);

    cudaFree(big_mat_dev);
    cudaFree(small_mat_dev);
}
