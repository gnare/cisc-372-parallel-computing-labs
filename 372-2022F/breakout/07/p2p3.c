#include <math.h>
#include <mpi.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define R_MAX 100

void gen_message(int *message, int *rank) {
  time_t t;
  srand((unsigned)time(&t) + *rank);

  *message = rand() % R_MAX;
}

int main() {
  int rank, nprocs;

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  int message;
  gen_message(&message, &rank);

  if (rank == 0) {
    printf("Process %d has: %d\n", rank, message);

    for (int i = 1; i < nprocs; i++) {
      MPI_Recv(&message, 1, MPI_INT, i, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      printf("Process %d has: %d\n", i, message);
    }
  } else {

    MPI_Send(&message, 1, MPI_INT, 0, rank, MPI_COMM_WORLD);
  }

  MPI_Finalize();
}
