#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
  int rank, nprocs;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (nprocs != 4) {
    fprintf(stderr, "Incorrect nprocs.\n");
    return -4;
  }
  
  if (rank == 1) {
    int a = 497;
    MPI_Send(&a, 1, MPI_INT, 3, 4, MPI_COMM_WORLD);
  } else if (rank == 3) {
    int b;
    MPI_Recv(&b, 1, MPI_INT, 1, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("Recieved by proc 3 from proc 1: %d\n", b);
  }

  MPI_Finalize();
}
