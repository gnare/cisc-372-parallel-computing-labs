#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  int rank, nprocs;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  srand(64 | rank);

  int randints[5];

  printf("Process %d has:      ", rank);
  for (int i = 0; i < 5; i++) {
    randints[i] = rand() % 1000;
    printf(" %4d", randints[i]);
  }
  printf("\n");
  fflush(stdout);

  int other_randints[5];
  if (rank == nprocs - 1) {
    MPI_Sendrecv(randints, 5, MPI_INT, 0, 0, other_randints, 5, 
      MPI_INT, rank - 1, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  } else if (rank == 0) {
    MPI_Sendrecv(randints, 5, MPI_INT, 1, 1, other_randints, 5, 
      MPI_INT, nprocs - 1, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  } else {
    MPI_Sendrecv(randints, 5, MPI_INT, rank + 1, rank + 1, other_randints, 5, 
      MPI_INT, rank - 1, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  }

  printf("Process %d recieved: ", rank);
  for (int i = 0; i < 5; i++) {
    printf(" %4d", other_randints[i]);
  }
  printf("\n");
  fflush(stdout);

  MPI_Finalize();
}
