#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  int rank, nprocs;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  
  int* vec = calloc(argc, sizeof(int));
  if (rank == 0) {
    for (int i = 0; i < argc; i++) {
      vec[i] = atoi(argv[i]);
    }

    for (int r = 1; r < nprocs; r++) {
      int first = argc * r / nprocs;
      int last = argc * (r + 1) / nprocs;
      int n = last - first;
      MPI_Send(&n, 1, MPI_INT, r, 41, MPI_COMM_WORLD);
      MPI_Send(vec+first, n, MPI_INT, r, 42, MPI_COMM_WORLD);
    }
    
  } else {
    int n = 0;
    MPI_Recv(&n, 1, MPI_INT, rank, 41, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("%d: ", n);
    int* vals = calloc(n, sizeof(int));
    MPI_Recv(vals, n, MPI_INT, rank, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    for(int i = 0; i < n; i++) {
      printf("%d", vals[i]);
    }
    printf("\n");
    free(vals);
  }

  free(vec);
  MPI_Finalize();
}
