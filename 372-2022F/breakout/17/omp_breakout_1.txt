
	       Breakout for Class 17: Beginning OpenMP

Copy this directory to your personal repo, breakout/17.

Work with the people at your table.

For this exercise, use only the following OpenMP constructs:

#pragma omp parallel
omp_get_num_threads()
omp_get_thread_num()

1. Knights (directory: knights)

In this directory is a silly program that takes any number of command
line arguments (strings) and prints something based on them.  Compile
and run it and see what it does.

Write a parallel version of this program using OpenMP.  Call it
knights_omp.c.  You may use only the following OpenMP constructs:

Parallelize as much of the program as possible.  Your parallel program
should be functionally equivalent to the original.

Figure out how to compile and run an OpenMP program on whatever
machine you are using.  For most compilers, "-fopenmp" is all you need
to compile.  To run on Beowulf using SLURM, use the -c flag to specify
the number of cores you are requesting.  The number of nodes is 1.
Here is an example:

srun --unbuffered -n 1 -c 4 ./hello_omp.exec 10

The --unbuffered will causes the printf output to appear on your
screen immediately (or at least sooner than it would otherwise).



2.  Pi (directory: pi)

Here is a more interesting program that computes an estimate of pi.
Parallelize it using OpenMP.  Here is one possible approach:

  (1) Figure out how many threads you have and store that number in
      global variable nthreads.  You can create a parallel region to
      do that only.  That should be the number of threads you get in
      any parallel region.

  (2) Allocate a buffer of long double of length nthreads.

  (3) Split up the work among threads using your favorite distribution
      scheme.   Thread i should write its result (a partial sum) to
      cell i of the buffer allocated in step 2.   Note each thread is
      writing to its own buffer cell, so there is no possibility of a
      data race.

  (4) After all threads are done, the original thread adds up the
      elements of the buffer and prints the result.
