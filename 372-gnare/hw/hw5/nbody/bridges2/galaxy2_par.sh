#!/bin/bash
#SBATCH -p RM
#SBATCH -t 00:05:00
#SBATCH -N 1
#SBATCH --ntasks-per-node 24
set -x
../galaxy2_par.exec 24 ../galaxy2_par_bridges.anim
