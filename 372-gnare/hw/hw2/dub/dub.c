#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLEN 64

char* dub(char* name) {
	const char* prefix = "Sir "; // len 5
	char* target = calloc(strnlen(name, MAXLEN) + 5, sizeof(char));
	
	strncat(target, prefix, 5);
	strncat(target, name, MAXLEN);
	
	return target;
}

int main(int argc, char** argv) {
	for (int i = 1; i < argc; i++) {
		char* dubbed = dub(argv[i]);
		printf("%s\n", dubbed);
		free(dubbed);
	}
	
	printf("\n");
}