#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
  MPI_Init(&argc, &argv);
  int rank, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  int n = argc-1;
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  int recvcount = (rank+1)*n/nprocs - rank*n/nprocs;
  int recvbuf[recvcount];
  if (rank == 0) {
    int sbuf[n];
    for (int i=0; i<n; i++)
      sbuf[i] = atoi(argv[i+1]);
    int sendcounts[nprocs];
    for (int i=0; i<nprocs; i++)
      sendcounts[i] = (int)((i+1)*n/nprocs) - (int)(i*n/nprocs);
    int displs[nprocs];
    displs[0] = 0;
    for (int i=1; i<nprocs; i++)
      displs[i] = displs[i-1]+sendcounts[i-1];
    MPI_Scatterv(sbuf, sendcounts, displs, MPI_INT,
		 recvbuf, recvcount, MPI_INT, 0, MPI_COMM_WORLD);
  } else {
    MPI_Scatterv(NULL, NULL, NULL, MPI_INT,
		 recvbuf, recvcount, MPI_INT, 0, MPI_COMM_WORLD);
  }
  MPI_Finalize();
  printf("Process %d has: ", rank);
  for (int i=0; i<recvcount; i++)
    printf(" %d", recvbuf[i]);
  printf("\n");
}
