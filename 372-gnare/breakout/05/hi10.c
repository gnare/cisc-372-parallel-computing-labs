#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	MPI_Init(&argc, &argv);
	
	for(int i = 0; i < 10; i++) {
		printf("%d: hi\n", i);
		fflush(stdout);
		printf("%d: bye\n", i);
		fflush(stdout);
	}
	
	return MPI_Finalize();
}
