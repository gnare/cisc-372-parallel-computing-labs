/* Fix the synchronization! */
#include <stdio.h>
#include <pthread.h>
#define N 10

pthread_mutex_t mutex;
pthread_cond_t cond_s1, cond_s2;

int s1 = 0, s2 = 0;

void * f1(void * arg) {
  for (int i=1; i<=N; i++) {
    pthread_mutex_lock(&mutex);
    printf("Thread 1: waiting for signal.\n");
    fflush(stdout);
    while (s1 < i) {
      pthread_cond_wait(&cond_s1, &mutex);
    }
    printf("Thread 1: signal received: s1=%d.\n", s1);
    fflush(stdout);
    s2 = i;
    pthread_cond_broadcast(&cond_s2);
    printf("Thread 1: sending signal.\n");
    fflush(stdout);
    pthread_mutex_unlock(&mutex);
  }
  printf("Thread 1: terminating.\n");
  fflush(stdout);
  return NULL;
}

void * f2(void * arg) {
  for (int i=1; i<=N; i++) {
    pthread_mutex_lock(&mutex);
    printf("\tThread 2: sending signal.\n");
    fflush(stdout);
    s1 = i;
    pthread_cond_broadcast(&cond_s1);
    printf("\tThread 2: waiting for signal.\n");
    fflush(stdout);
    while(s2 < i){
      pthread_cond_wait(&cond_s2, &mutex);
    }
    printf("\tThread 2: signal received: s2=%d.\n", s2);
    fflush(stdout);
    pthread_mutex_unlock(&mutex);
  }
  printf("\tThread 2: terminating.\n");
  fflush(stdout);
  return NULL;
}

int main() {
  pthread_t t1, t2;
  pthread_mutex_init(&mutex, NULL);
  pthread_create(&t2, NULL, f2, NULL);
  pthread_create(&t1, NULL, f1, NULL);
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  pthread_mutex_destroy(&mutex);
}
