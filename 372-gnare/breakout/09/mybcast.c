#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  int rank, nprocs;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  int myints[argc - 1];
  int n = argc - 1;  

  if (rank == 0) {
    for (int i = 0; i < argc - 1; i++) {
      myints[i] = atoi(argv[i + 1]);
    }
  }

  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(myints, argc - 1, MPI_INT, 0, MPI_COMM_WORLD);
  
  printf("Process %d recieved: ", rank);
  for (int i = 0; i < n; i++) {
    printf(" %4d", myints[i]);
  }
  printf("\n");
  fflush(stdout);

  MPI_Finalize();
}
