#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>

double s = 0;
pthread_mutex_t mutexlock;
int nthreads;
double * nums;
int length;

void * maxer(void * arg) {
  int tid = *((int*)arg);
  int first = (tid*length)/nthreads;

  double max = nums[first];
  int last = ((tid+1)*length)/nthreads;

  for (int i = first+1; i < last; i++) {
    if (nums[i] > max)
      max = nums[i];
  }
  pthread_mutex_lock(&mutexlock);
  if (max > s)
    s = max;
  pthread_mutex_unlock(&mutexlock);
  return NULL;
}

void find_max(int len, double * a, int nthreads) {
  pthread_t threads[nthreads];
  int tids[nthreads];
  pthread_mutex_init(&mutexlock,NULL);

  for (int i = 0; i < nthreads; i++)
    tids[i] = i;
  for (int i = 0; i < nthreads; i++)
    pthread_create(threads+i, NULL, maxer, tids + i);
  for (int i = 0; i < nthreads; i++)
    pthread_join(threads[i], NULL);

  pthread_mutex_destroy(&mutexlock);
}

int main(int argc, char * argv[]) {
  assert(argc>2);
  nthreads = atoi(argv[1]);
  assert(nthreads>=0);
  length = argc-2;

  nums = malloc((argc-2)*sizeof(double));

  for (int i = 0; i < argc-2; i++)
    nums[i] = atof(argv[i+2]);

  find_max(length, nums, nthreads);
  printf("The max is %lf\n", s);
}
