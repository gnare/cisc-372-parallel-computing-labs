/* Incorrect synchronization.  Use mutexes and condition variables
 * to fix. */

// Worked on by Ellie Noonan, Macy Beach, Junnan Bai, and Braxton Madara

#include <stdio.h>
#include <pthread.h>

int s = 0;
pthread_mutex_t mutex;
pthread_cond_t notZero;

void * f1(void * arg) {
  //printf("Entered fi\n");
  pthread_mutex_lock(&mutex);
  printf("Thread 1: waiting for signal.\n");
  fflush(stdout);
  while(s == 0){
    pthread_cond_wait(&notZero, &mutex);
  }
  printf("Thread 1: signal received: s=%d\n", s);
  fflush(stdout);
  pthread_mutex_unlock(&mutex);
  return NULL;
}

void * f2(void * arg) {
  printf("\tThread 2: sending signal.\n");
  fflush(stdout);
  pthread_mutex_lock(&mutex);
  s = 1;
  pthread_cond_signal(&notZero);
  pthread_mutex_unlock(&mutex);
  return NULL;
}

int main() {
  pthread_t t1, t2;
  pthread_cond_init(&notZero, NULL);
  //printf("Initialization over\n");
  pthread_create(&t1, NULL, f1, NULL);
  pthread_create(&t2, NULL, f2, NULL);
  //printf("Creation over\n");
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  //printf("Joining over\n");
  pthread_cond_destroy(&notZero);
  pthread_mutex_destroy(&mutex);
  //printf("Destruction over\n");
}
