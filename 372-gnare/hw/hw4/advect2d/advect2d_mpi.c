#include <stdlib.h>
#include <assert.h>
#include <mpi.h>
#include <sys/time.h>
#include "mpianim.h"

const int n = 200;         // number of discrete points including endpoints
const double k = 0.01;     // D*dt/(dx*dx), diffusivity constant
const double c = 0.001;    // advection constant
const int nstep = 200000;  // number of time steps
const int wstep = 400;     // time between writes to file
const double m = 100.0;    // initial temperature of interior

char * filename = "advect2d_mpi.anim";  // name of file to create
double ** u, ** u_new;     // two copies of the temperature function
MPIANIM_File af;           // output file
double start_time;         // time simulation starts

int nprocs, rank;
int first, nl;
int left, right;

double* ghost_left;     // Holding varaible for ghost column L
double* ghost_right;     // Holding varaible for ghost column R

double min = m;
double sum = 0.0;

double mytime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return t.tv_sec + t.tv_usec / 1000000.0;
}

static void setup(void) {
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  first = rank * n / nprocs;
  nl = (rank + 1) * n / nprocs - first;
  start_time = ANIM_time();
  u = ANIM_allocate2d(nl + 2, n);
  u_new = ANIM_allocate2d(nl + 2, n);

  int h0 = n / 2 - n / 3;
  int h1 = n / 2 + n / 3;

  // remap to local
  int h0_loc = h0 - first;
  int h1_loc = h1 - first;

  for (int i = 0; i < nl + 2; i++) {
    for (int j = 0; j < n; j++) {
        u_new[i][j] = u[i][j] = 0.0;  
    }
  }

  // Initial condition
  if (h0_loc >= 0 && h0_loc < nl) {
    for (int i = h0_loc; i < h1_loc && i < nl + 1; i++) {
      for (int j = h0; j < h1; j++) {
        u_new[i][j] = u[i][j] = m;
      }
    }
  } else if (h0_loc < 0 && h1_loc >= 0) {
    for (int i = 0; i < h1_loc && i < nl + 1; i++) {
      for (int j = h0; j < h1; j++) {
        u_new[i][j] = u[i][j] = m;
      }
    }
  }

  af = MPIANIM_Create_heat_2d(n, n, 0, 1, 0, 1, 0, m,
      nl, n, first, 0, filename, MPI_COMM_WORLD);
}

static void teardown(void) {
  MPIANIM_Close(af);

  ANIM_free2d(u);
  ANIM_free2d(u_new);

  if (rank == 0) {
    printf("\n");
  }
}

static void exchange() {
  ghost_left = calloc(n, sizeof(double));
  ghost_right = calloc(n, sizeof(double));

  if (rank == 0) {
    MPI_Send(u[nl], n, MPI_DOUBLE, 1, 1, MPI_COMM_WORLD);
    MPI_Send(u[1], n, MPI_DOUBLE, nprocs - 1, 0, MPI_COMM_WORLD);

    MPI_Recv(ghost_right, n, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(ghost_left, n, MPI_DOUBLE, nprocs - 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  } else if (rank == nprocs - 1) {
    MPI_Send(u[nl], n, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(u[1], n, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD);

    MPI_Recv(ghost_right, n, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(ghost_left, n, MPI_DOUBLE, rank - 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  } else {
    MPI_Send(u[nl], n, MPI_DOUBLE, rank + 1, 1, MPI_COMM_WORLD);
    MPI_Send(u[1], n, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD);

    MPI_Recv(ghost_right, n, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(ghost_left, n, MPI_DOUBLE, rank - 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  }

  for (int j = 0; j < n; j++) {
    u[0][j] = ghost_left[j];
    u[nl + 1][j] = ghost_right[j];
  }

  free(ghost_left);
  free(ghost_right);
}

static void update() {
  // for the love of everything that is holy *please* stop using bracketless code blocks
  // also the template indents: WHY
  for (int i = 1; i < nl + 1; i++) {
    for (int j = 0; j < n; j++) {
      u_new[i][j] =  u[i][j] + k * (u[(i + 1) % n][j]
        + u[(i + n - 1) % n][j] + u[i][(j + 1) % n]
        + u[i][(j + n - 1) % n] - 4 * u[i][j])
        - c * (u[(i + 1) % n][j] - u[(i + n - 1) % n][j]
        + u[i][(j + 1) % n] - u[i][(j + n - 1) % n]);
    }
  }

  double ** const tmp = u_new;
  u_new = u;
  u = tmp;
}

int main() {
  start_time = mytime();

  int dots = 0;
  MPI_Init(NULL, NULL);
  setup();

  if (wstep != 0) {
    MPIANIM_Write_frame(af, &u[1][0], MPI_STATUS_IGNORE);
  }
  
  for (int i = 1; i <= nstep; i++) {
    exchange();
    update();
    if (rank == 0) {
      ANIM_Status_update(stdout, nstep, i, &dots);
    }

    // Calc final values
    if (i == nstep) {
      for (int i = 1; i < nl + 1; i++) {
        for (int j = 0; j < n; j++) {
          min = (u[i][j] < min) ? u[i][j] : min;
          sum += u[i][j];
        }
      }

      double final_min = 0.0;
      double final_sum = 0.0;
      MPI_Reduce(&min, &final_min, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
      MPI_Reduce(&sum, &final_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

      if (rank == 0) {
        double mean = final_sum / (n * n);
        double final_time = mytime() - start_time;
        printf("\nTime (s) = %.6f, Mean = %.6f, Min = %.6f\n", final_time, mean, final_min);
      }
    }

    if (wstep != 0 && i % wstep == 0) {
      MPIANIM_Write_frame(af, &u[1][0], MPI_STATUS_IGNORE);
    }
  }

  teardown();
  MPI_Finalize();
}
