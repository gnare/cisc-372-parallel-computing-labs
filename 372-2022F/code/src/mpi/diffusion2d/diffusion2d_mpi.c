/* diffusion2d_mpi.c: MPI version of diffusion2d.c.   The first coordinate
   of the 2d array is distributed in blocks.
   Author: Stephen F. Siegel
*/
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <mpi.h>
#include "mpianim.h"
#define ulong unsigned long
#define FIRST(rank) ((((ulong)(rank))*((ulong)nx))/nprocs)
#define OWNER(index) ((((ulong)nprocs)*(((ulong)(index))+1)-1)/nx)

/* Global variables */
int nx, ny;               /* dimensions of the room (in pixels) */
double k;                 /* constant controlling rate of diffusion */
int nstep;                /* number of time steps */
int wstep;                /* time between writes to file */
char * filename;          /* name of file to create */
double ** u, ** u_new;    /* two copies of temperature function */
MPIANIM_File af;          /* output file */
double start_time;        /* time simulation starts */
int nprocs, rank;         /* number of processes, rank of this process */
int left, right;          /* rank of left, right neighbor or MPI_PROC_NULL */
int nxl;                  /* number of cells "owned" by this proc */
int first;                /* global index of first cell owned by this proc */
int start, stop;          /* first and last local index to update */

static void quit() {
  printf("Usage: mpiexec -n NP diffusion2d_mpi.exec NX NY K NSTEPS WSTEP\
 FILENAME 								\n\
  NX = number of pixles in x-direction                                  \n\
  NY = number of pixles in y-direction                                  \n\
  K =  a constant controlling rate of diffusion in (0,.25)              \n\
  NSTEP = total number of time steps, at least 1                        \n\
  WSTEP = number of time steps between writes to file, in [0, NSTEP]    \n\
  FILENAME = name of output file                                        \n\
Example: mpiexec -n 4 diffusion2d_mpi.exec 400 400 0.2 50000 100 out.anim\n");
  exit(1);
}

static void setup(int argc, char * argv[]) {
  int filename_length = 0, nframes = 0;

  MPI_Barrier(MPI_COMM_WORLD);
  start_time = MPI_Wtime();
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    if (argc != 7) quit();
    nx = atoi(argv[1]), ny = atoi(argv[2]), k = atof(argv[3]),
      nstep = atoi(argv[4]), wstep = atoi(argv[5]), filename = argv[6];
    if (!(nx>=6 && ny>=6 && 0<k && k<.25 && nstep>=1
	  && wstep>=0 && wstep<=nstep))
      quit();
    nframes = wstep == 0 ? 0 : 1+nstep/wstep;
    filename_length = strlen(filename);
    printf("diffusion2d_mpi: nx=%d ny=%d k=%.3lf nstep=%d wstep=%d nprocs=%d\n",
	   nx, ny, k, nstep, wstep, nprocs);
    printf("diffusion2d_mpi: creating ANIM file %s with %d frames, %zu bytes.\n",
	   filename, nframes, ANIM_Heat_file_size_2d(nx, ny, nframes));
    fflush(stdout);
  }
  int tmp[] = {nx, ny, nstep, wstep, nframes, filename_length};
  MPI_Bcast(tmp, 6, MPI_INT, 0, MPI_COMM_WORLD);
  if (rank != 0) {
    nx=tmp[0], ny=tmp[1], nstep=tmp[2], wstep=tmp[3], nframes=tmp[4],
      filename_length=tmp[5];
    filename = malloc(filename_length + 1);
    assert(filename);
  }
  MPI_Bcast(&k, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(filename, filename_length + 1, MPI_CHAR, 0, MPI_COMM_WORLD);
  first = FIRST(rank);
  nxl = FIRST(rank+1) - first;
  left = (first == 0 || nxl == 0 ? MPI_PROC_NULL : OWNER(first - 1));
  right = (first + nxl >= nx || nxl == 0 ? MPI_PROC_NULL : OWNER(first + nxl));
  u = ANIM_allocate2d( nxl + 2, ny );
  u_new = ANIM_allocate2d( nxl + 2, ny );
  if (rank == OWNER(0)) { // left-most proc owning at least one column
    for (int j=0; j<ny; j++)
      u[1][j] = u_new[1][j] = 0.0;
    start = 2; // u[2] is the first column to be updated
  } else {
    start = 1; // u[1] is the first column to be updated
  }
  if (rank == OWNER(nx-1)) { // right-most proc owning at least one column
    for (int j=0; j<ny;j++)
      u[nxl][j] = u_new[nxl][j] = 0.0;
    stop = nxl - 1; // u[nxl-1] is the last cell to be updated
  } else {
    stop = nxl; // u[nxl] is the last cell to be updated
  }
  for (int i = start; i <= stop; i++) {
    u[i][0] = u_new[i][0] = u[i][ny-1] = u_new[i][ny-1] = 0.0;
    for (int j = 1; j < ny - 1; j++)
      u[i][j] = 100.0;
  }
  af = MPIANIM_Create_heat_2d(nx, ny, 0, nx, 0, ny, 0, 100.0,
			      nxl, ny, first, 0, filename, MPI_COMM_WORLD);
  MPIANIM_Set_nframes(af, nframes);
}

static void teardown() {
  MPIANIM_Close(af);
  ANIM_free2d(u);
  ANIM_free2d(u_new);
  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 0)
    printf("\ndiffusion2d_mpi: finished.  Time = %lf\n",
	   MPI_Wtime() - start_time);
  else
    free(filename);
}

static void exchange_ghost_cells() {
  MPI_Sendrecv(u[1], ny, MPI_DOUBLE, left, 0, u[nxl+1], ny, MPI_DOUBLE,
	       right, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  MPI_Sendrecv(u[nxl], ny, MPI_DOUBLE, right, 0, u[0], ny, MPI_DOUBLE,
	       left, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

void update() {
  for (int i = start; i <= stop; i++)
    for (int j = 1; j < ny-1; j++)
      u_new[i][j] = u[i][j] +
	k*(u[i+1][j] + u[i-1][j] + u[i][j+1] + u[i][j-1] - 4*u[i][j]);
  double ** const tmp = u_new; u_new = u; u = tmp;
}

int main(int argc, char *argv[]) {
  int dots = 0; // number of dots printed so far (0..100)

  MPI_Init(&argc, &argv);
  setup(argc, argv);
  if (wstep != 0) MPIANIM_Write_frame(af, u[1], MPI_STATUS_IGNORE);
  for (int i = 1; i <= nstep; i++) {
    exchange_ghost_cells();
    update();
    if (rank == 0) ANIM_Status_update(stdout, nstep, i, &dots);
    if (wstep != 0 && i%wstep == 0)
      MPIANIM_Write_frame(af, u[1], MPI_STATUS_IGNORE);
  }
  teardown();
  MPI_Finalize();
}
