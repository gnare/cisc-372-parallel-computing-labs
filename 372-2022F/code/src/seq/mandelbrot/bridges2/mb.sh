#!/bin/bash
#SBATCH -p RM-shared
#SBATCH -t 00:10:00
#SBATCH -N 1
#SBATCH --ntasks-per-node 1
# echo commands to stdout
set -x
./mandelbrot.exec 1200 400 mb_big.anim
