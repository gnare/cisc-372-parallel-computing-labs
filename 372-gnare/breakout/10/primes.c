#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  int rank, nprocs;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (argc < 2) {
    fprintf(stderr, "Too few arguments.\n");
    return -1;
  }

  int max = atoi(argv[1]);

  // setup distribution alg
  int avail[nprocs];
  for (int i = 1; i < nprocs; i++) {
    avail[i] = 1;
  }

  if (rank == 0) {
    for (int i = 1; i <= max; i++) {
        // TODO check availability of workers and distribute
      for (int j = 1; j < nprocs; j++) {
        if (!avail[j]) {
            
        } else {
            MPI_Send(&i, 1, MPI_INT, j, 0, MPI_COMM_WORLD);
            avail[j] = 0;
            break;
        }
      }
    }

    // Send end of stream message
    for (int j = 1; j < nprocs; j++) {
        int eos_buf = -1;
        MPI_Send(&eos_buf, 1, MPI_INT, j, 0, MPI_COMM_WORLD);
    }
  }


// old boilerplate...ignore  
  printf("Process %d recieved: ", rank);
  for (int i = 0; i < n; i++) {
    printf(" %4d", myints[i]);
  }
  printf("\n");
  fflush(stdout);

  MPI_Finalize();
}
