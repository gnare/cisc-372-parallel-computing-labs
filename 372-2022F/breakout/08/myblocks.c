#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>

int main(int argc, char* argv[]){

	MPI_Init(&argc, &argv);
	int rank, nprocs, n = argc-1;

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);	
	
	if (rank == 0){
		// make array of ints from args
		int ints[n];
		for (int i=1; i<=n; i++){
			ints[i-1] = atoi(argv[i]);
		}

		for(int i=1; i<nprocs; i++){
			// send number of elements total to each proc
			MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
			// send each process the right block of nums
			int *blockedArr = &ints[(int)(floor(n*i/nprocs))];
			int blockSize = floor(n*(i+1)/nprocs) - floor(n*(i)/nprocs);
			MPI_Send(blockedArr, blockSize, MPI_INT, i, i, MPI_COMM_WORLD);
		}
		// give proc 0 its block of nums
		int *blockedArr = &ints[0];
		int blockSize = floor(n/nprocs);	
		printf("Process %d's block: ", rank);
		for(int i=0; i<blockSize; i++){
			printf("%d ", blockedArr[i]);
		} printf("\n");

	} else {
		// recv the number of elements
		MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		int blockSize = floor(n*(rank+1)/nprocs) - floor(n*rank/nprocs); //get the size of this procs array block
		int *arr = malloc(sizeof(int) * blockSize); //allocate space for array block
		// recv the array data
		MPI_Recv(arr, blockSize, MPI_INT, 0, rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		// print elements
		printf("Process %d's block: ", rank);
		for(int i=0; i<blockSize; i++){
			printf("%d ", arr[i]);
		} printf("\n");
	}

	MPI_Finalize();

}
