#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>

int nthreads;
pthread_mutex_t mutexLock;
int sum = 0;

void *runner(void *arg) {
  int *tipd = (int *)arg;
  int returnCode = pthread_mutex_lock(&mutexLock);
  printf("The error code is: %d\n", returnCode);
  sum += (*tipd) + 1;
  return NULL;
}

int main(int argc, char *argv[]) {
  assert(argc >= 2);
  nthreads = atoi(argv[1]);
  pthread_t threads[nthreads];
  pthread_mutex_init(&mutexLock, NULL);
  int tids[nthreads];
  for (int i = 0; i < nthreads; i++) {
    tids[i] = i;
  }
  for (int i = 0; i < nthreads; i++) {
    pthread_create(threads + i, NULL, runner, tids + i);
  }
  for (int i = 0; i < nthreads; i++) {
    pthread_join(threads[i], NULL);
  }
  printf("The sum is %d\n", sum);
  return 0;
}
