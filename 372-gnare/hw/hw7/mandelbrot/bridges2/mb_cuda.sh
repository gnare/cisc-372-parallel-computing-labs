#!/bin/bash
#SBATCH -p GPU-shared
#SBATCH -t 00:05:00
#SBATCH -N 1
#SBATCH --gpus=1
set -x
../mandelbrot_cuda.exec 1200 400 out.anim
