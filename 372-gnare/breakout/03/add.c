#include <stdio.h>
#include <stdlib.h>

double final_sum = 0.0;

void add(double* sum, double i) {
	*sum += i;
} 

int main(int argc, char** argv) {
	for (int i = 1; i < argc; i++) {
		add(&final_sum, atof(argv[i]));		
	}

	printf("%f\n", final_sum);
}
