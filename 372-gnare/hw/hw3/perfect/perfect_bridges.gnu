set terminal pdf  # size 4, 4
# set tmargin at screen 0.9
# set lmargin at screen 0.1
# set rmargin at screen -0.1
# set bmargin at screen 0.1
# set size ratio 1.2
set output "perfect_bridges.pdf"
# unset log
# unset label
# set key vertical top left 
set xlabel center "Number of processes"
set ylabel center "time (seconds)"
# set logscale y
# set format y "10^{%T}"
# set xtics 1, 1, 10
# set ytics 
set xr [0:140]
set yr [0:150]
plot "bridges.dat" using 1:2 title 'MPI Bridges Perfect' with linespoints
