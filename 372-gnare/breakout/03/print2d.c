#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	if (argc != 3) {
		printf("wrong number of args\n");
		return -1;
	} else {
		int n = atoi(argv[1]);
		int m = atoi(argv[2]);
		double arr[n][m];
		for (int n_i = 0; n_i < n; n_i++) {
			for (int m_i = 0; m_i < m; m_i++) {
				if (n_i == 0 || m_i == 0 || n_i == n - 1 || m_i == m - 1) {
					arr[n_i][m_i] = 100.0;
				} else {
					arr[n_i][m_i] = 0.0;
				}
				printf("%4.1f ", arr[n_i][m_i]);
			}
			printf("\n");
		}
	}
}
